﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

using Discord;
using Discord.WebSocket;

namespace Furry.e621 {

	/// <summary>
	/// This readonly struct represents a request for e621.net.
	/// It holds everything needed to pull data from said site and a neat struct.
	/// It also allows for a callback method to be used.
	/// </summary>
	public abstract class Request {

		/// <summary>
		/// The message channel to respond to.
		/// </summary>
		public IMessageChannel MessageChannel {
			get;
		}

		/// <summary>
		/// The user that made the request.
		/// </summary>
		public SocketUser User {
			get;
		}

		/// <summary>
		/// The method to call back to when the request is done.
		/// </summary>
		public Action<List<Post>, Request> Callback {
			get;
		}

		/// <summary>
		/// Builds the struct and uses a callback to signal the that request is done.
		/// This callback method must accept both the list of posts and the struct itself.
		/// </summary>
		/// <param name="user">The Discord user that made the request.</param>
		/// <param name="messageChannel">The message channel the user used.</param>
		/// <param name="callback">The method to report to when the request is done.</param>
		/// <param name="tags">The tags for the request to be used.</param>
		protected Request( SocketUser user, IMessageChannel messageChannel, Action<List<Post>, Request> callback ) {
			Callback = callback;
			User = user;
			MessageChannel = messageChannel;
		}

	}

	/// <summary>
	/// Represents a request to pull a random post from e621.net.
	/// It extends <see cref="Request"/> to make it easier to use a single queue to pull.
	/// Because e621.net has a rate limit of 1 request per 2 seconds for all API calls from a single User-Agent.
	/// </summary>
	public class PostRequest : Request {

		/// <summary>
		/// Holds a collection of readonly strings that represents the tags.
		/// </summary>
		public ReadOnlyCollection<string> Tags {
			get;
		}

		public PostRequest( SocketUser user, IMessageChannel messageChannel, Action<List<Post>, Request> callback, params string[] tags )
			: base( user, messageChannel, callback ) {
			Tags = new ReadOnlyCollection<string>( tags );
		}
	}

	/// <summary>
	/// Represents a request to pull a post that has been linked in chat.
	/// It extends <see cref="Request"/> to make it easier to use a single queue to pull.
	/// Because e621.net has a rate limit of 1 request per 2 seconds for all API calls from a single User-Agent.
	/// </summary>
	public class LinkRequest : Request {

		public string Link {
			get;
		}

		public ulong MessageId {
			get;
		}

		public new Action<SocketUser, IMessageChannel, string, ulong> Callback {
			get;
		}

		public LinkRequest( SocketUser user, IMessageChannel messageChannel, Action<SocketUser, IMessageChannel, string, ulong> callback, string link, ulong messageId )
			: base( user, messageChannel, null ) {
			Callback = callback;
			MessageId = messageId;
			Link = link;
		}

		public LinkRequest( SocketUserMessage message, Action<SocketUser, IMessageChannel, string, ulong> callback, string link )
			: this( message.Author, message.Channel, callback, link, message.Id ) {

		}
	}
}
