﻿using System;
using System.Runtime.Serialization;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Furry.e621 {

	[JsonConverter( typeof( StringEnumConverter ) )]
	public enum Rating : byte {
		[EnumMember( Value = "s" )]
		Safe,
		[EnumMember( Value = "q" )]
		Questionable,
		[EnumMember( Value = "e" )]
		Explicit
	}

	public struct Flags {

		private const byte PENDING_MASK = 0b00_000001;
		private const byte FLAGGED_MASK = 0b00_000010;
		private const byte NOTE_LOCKED_MASK = 0b00_000100;
		private const byte STATUS_LOCKED_MASK = 0b00_001000;
		private const byte RATING_LOCKED_MASK = 0b00_010000;
		private const byte DELETED_MASK = 0b00_100000;

		public bool Pending => Convert.ToBoolean( boolFlags & PENDING_MASK );

		public bool Flagged => Convert.ToBoolean( ( boolFlags & FLAGGED_MASK ) >> 1 );

		public bool NoteLocked => Convert.ToBoolean( ( boolFlags & NOTE_LOCKED_MASK ) >> 2 );

		public bool StatusLocked => Convert.ToBoolean( ( boolFlags & STATUS_LOCKED_MASK ) >> 3 );

		public bool RatingLocked => Convert.ToBoolean( ( boolFlags & RATING_LOCKED_MASK ) >> 4 );

		public bool Deleted => Convert.ToBoolean( ( boolFlags & DELETED_MASK ) >> 5 );

		private byte boolFlags;

		[JsonConstructor]
		internal Flags( bool pending, bool flagged, bool note_locked, bool status_locked, bool rating_locked, bool deleted ) {
			var pendingByte = Convert.ToByte( pending );
			var flaggedByte = Convert.ToByte( flagged );
			var noteLockedByte = Convert.ToByte( note_locked );
			var statusLockedByte = Convert.ToByte( status_locked );
			var ratingLockedByte = Convert.ToByte( rating_locked );
			var deletedByte = Convert.ToByte( deleted );

			boolFlags = ( byte )( ( pendingByte << 0 ) | ( flaggedByte << 1 ) | ( noteLockedByte << 2 ) | ( statusLockedByte << 3 ) | ( ratingLockedByte << 4 ) | ( deletedByte << 5 ) );
		}

	}

	public struct Score {

		public int Up {
			get;
		}

		public int Down {
			get;
		}

		public int Total {
			get;
		}

		[JsonConstructor]
		internal Score( int up, int down, int total ) {
			Up = up;
			Down = down;
			Total = total;
		}

	}

	public class File {

		public uint Width {
			get;
		}

		public uint Height {
			get;
		}

		public ulong SizeRaw {
			get;
		}

		public string Size {
			get {
				var loops = ( byte )0;
				var reducedSize = ( double )SizeRaw;

				for ( ; reducedSize > 1000f; ++loops ) {
					reducedSize /= 1024f;
				}

				switch ( loops ) {
					case 0:
						return $"{( ulong )reducedSize} Bytes";
					case 1:
						return ReturnStringCreator( reducedSize, "KiB" );
					case 2:
						return ReturnStringCreator( reducedSize, "MiB" );
					case 3:
						return ReturnStringCreator( reducedSize, "GiB" );
					case 4:
						return ReturnStringCreator( reducedSize, "TiB" );
					case 5:
						return ReturnStringCreator( reducedSize, "PiB" );
					default:
						throw new Exception( "File too big." );
				}

				static string ReturnStringCreator( double val, string step ) => $"{val:0.00} {step}";
			}
		}

		public string MD5 {
			get;
		}

		public string Extension {
			get;
		}

		public Uri URL {
			get;
		}

		[JsonConstructor]
		internal File( uint width, uint height, ulong size, string md5, string ext, string url )
			: this( string.IsNullOrWhiteSpace( url ) ? null : new Uri( url ), width, height, size, md5, ext ) { }

		internal File( Uri url, uint width, uint height, ulong size, string md5, string ext ) {
			Width = width;
			Height = height;
			SizeRaw = size;
			MD5 = md5;
			Extension = ext;
			URL = url;
		}

	}

	public class Preview {

		public uint Width {
			get;
		}

		public uint Height {
			get;
		}

		public Uri URL {
			get;
		}

		[JsonConstructor]
		internal Preview( uint width, uint height, string url ) : this( string.IsNullOrWhiteSpace( url ) ? null : new Uri( url ), width, height ) {

		}

		internal Preview( Uri url, uint width, uint height ) {
			Width = width;
			Height = height;
			URL = url;
		}
	}

	public class Sample {

		public bool? HasSample {
			get;
		}

		public uint Width {
			get;
		}

		public uint Height {
			get;
		}

		public Uri URL {
			get;
		}

		[JsonConstructor]
		internal Sample( bool? has, uint width, uint height, string url ) : this( string.IsNullOrWhiteSpace( url ) ? null : new Uri( url ), has, width, height ) {

		}

		internal Sample( Uri url, bool? hasSample, uint width, uint height ) {
			HasSample = hasSample;
			Width = width;
			Height = height;
			URL = url;
		}
	}

	public class Tags {

		public ReadOnlyMemory<string> General => new ReadOnlyMemory<string>( general );

		public ReadOnlyMemory<string> Species => new ReadOnlyMemory<string>( species );

		public ReadOnlyMemory<string> Character => new ReadOnlyMemory<string>( character );

		public ReadOnlyMemory<string> Copyright => new ReadOnlyMemory<string>( copyright );

		public ReadOnlyMemory<string> Artist => new ReadOnlyMemory<string>( artist );

		public ReadOnlyMemory<string> Invalid => new ReadOnlyMemory<string>( invalid );

		public ReadOnlyMemory<string> Lore => new ReadOnlyMemory<string>( lore );

		public ReadOnlyMemory<string> Meta => new ReadOnlyMemory<string>( meta );

		public ReadOnlyMemory<string> AllTags {
			get {
				var tags = new string[general.Length + species.Length + character.Length + copyright.Length + artist.Length + invalid.Length + lore.Length + meta.Length];
				var part1 = general.Length + species.Length;
				var part2 = part1 + character.Length;
				var part3 = part2 + copyright.Length;
				var part4 = part3 + artist.Length;
				var part5 = part4 + invalid.Length;
				var part6 = part5 + lore.Length;

				for ( var i = 0; i < tags.Length; ++i ) {
					if ( i < general.Length ) {
						tags[i] = general[i];
					} else if ( i < part1 ) {
						tags[i] = species[i - general.Length];
					} else if ( i < part2 ) {
						tags[i] = character[i - part1];
					} else if ( i < part3 ) {
						tags[i] = copyright[i - part2];
					} else if ( i < part4 ) {
						tags[i] = artist[i - part3];
					} else if ( i < part5 ) {
						tags[i] = invalid[i - part4];
					} else if ( i < part6 ) {
						tags[i] = lore[i - part5];
					} else {
						tags[i] = meta[i - part6];
					}
				}

				return new ReadOnlyMemory<string>( tags );
			}
		}

		public ReadOnlyMemory<string> AllTagsWithoutArtist {
			get {
				var tags = new string[general.Length + species.Length + character.Length + copyright.Length + invalid.Length + lore.Length + meta.Length];
				var part1 = general.Length + species.Length;
				var part2 = part1 + character.Length;
				var part3 = part2 + copyright.Length;
				var part4 = part3 + invalid.Length;
				var part5 = part4 + lore.Length;

				for ( var i = 0; i < tags.Length; ++i ) {
					if ( i < general.Length ) {
						tags[i] = general[i];
					} else if ( i < part1 ) {
						tags[i] = species[i - general.Length];
					} else if ( i < part2 ) {
						tags[i] = character[i - part1];
					} else if ( i < part3 ) {
						tags[i] = copyright[i - part2];
					} else if ( i < part4 ) {
						tags[i] = invalid[i - part3];
					} else if ( i < part5 ) {
						tags[i] = lore[i - part4];
					} else {
						tags[i] = meta[i - part5];
					}
				}

				return new ReadOnlyMemory<string>( tags );
			}
		}

		private readonly string[] general;
		private readonly string[] species;
		private readonly string[] character;
		private readonly string[] copyright;
		private readonly string[] artist;
		private readonly string[] invalid;
		private readonly string[] lore;
		private readonly string[] meta;

		[JsonConstructor]
		internal Tags( string[] general, string[] species, string[] character, string[] copyright, string[] artist, string[] invalid, string[] lore, string[] meta )
			: this( new string[8][] { general, species, character, copyright, artist, invalid, lore, meta } ) {
		}

		internal Tags( params string[][] tags ) {
			if ( tags.Length != 8 ) {
				throw new ArgumentException( $"Expected 8 arrays, but got `{tags.Length}`.", nameof( tags ) );
			}

			if ( AnyArrayNull( tags ) ) {
				throw new ArgumentException( $"A secondary array is null when it should be empty.", nameof( tags ) );
			}

			general = tags[0];
			species = tags[1];
			character = tags[2];
			copyright = tags[3];
			artist = tags[4];
			invalid = tags[5];
			lore = tags[6];
			meta = tags[7];

			static bool AnyArrayNull( params string[][] arrs ) {
				for ( var i = 0; i < arrs.Length; ++i ) {
					if ( arrs[i] is null ) {
						return true;
					}
				}

				return false;
			}
		}
	}

	public class Relationship {

		public bool HasChildren {
			get;
		}

		public bool HasActiveChildren {
			get;
		}

		public uint? ParentID {
			get;
		}

		public ReadOnlyMemory<uint> Children => new ReadOnlyMemory<uint>( children );

		private uint[] children;

		[JsonConstructor]
		internal Relationship( bool has_children, bool has_active_children, uint? parent_id, uint[] children ) {
			if ( children is null ) {
				throw new ArgumentNullException( nameof( children ), "Cannot be null, but either have things in it or empty." );
			}

			HasChildren = has_children;
			HasActiveChildren = has_active_children;
			ParentID = parent_id;
			this.children = new uint[children.Length];

			Array.Copy( children, this.children, children.Length );
		}
	}

	public class Post {

		public bool IsFavorited {
			get;
		}

		public uint ID {
			get;
		}

		public uint ChangeSequence {
			get;
		}

		public uint FavouriteCount {
			get;
		}

		public uint? ApproverID {
			get;
		}

		public uint UploaderID {
			get;
		}

		public uint CommentCount {
			get;
		}

		public string Description {
			get;
		}

		public Rating Rating {
			get;
		}

		public ReadOnlyMemory<uint> PoolIDs => new ReadOnlyMemory<uint>( poolIds );

		public ReadOnlyMemory<string> LockedTags => new ReadOnlyMemory<string>( lockedTags );

		public DateTimeOffset Created {
			get;
		}

		public DateTimeOffset? Updated {
			get;
		}

		public ReadOnlyMemory<Uri> Sources => new ReadOnlyMemory<Uri>( sources );

		public Score Score {
			get;
		}

		public Flags Flags {
			get;
		}

		public File File {
			get;
		}

		public Preview Preview {
			get;
		}

		public Sample Sample {
			get;
		}

		public Tags Tags {
			get;
		}

		public Relationship Relationship {
			get;
		}

		public Uri PostURL => new Uri( $"http://{( Rating == Rating.Safe ? "e926" : "e621" )}.net/posts/{ID}" );

		private uint[] poolIds;
		private string[] lockedTags;

		private Uri[] sources;

		// Yes, yes, I know constructors are not normally laid out like this,
		// but goddamn is there so much and this makes it more readable.
		[JsonConstructor]
		internal Post(
			bool isFavorited,
			uint id,
			uint change_seq,
			uint fav_count,
			uint? approver_id,
			uint uploader_id,
			uint comment_count,
			string description,
			Rating rating,
			DateTimeOffset created_at,
			DateTimeOffset? updated_at,
			Score score,
			Flags flags,
			File file,
			Preview preview,
			Sample sample,
			Relationship relationships,
			uint[] pools,
			string[] locked_tags,
			Uri[] sources,
			Tags tags
		) {
			IsFavorited = isFavorited;
			ID = id;
			ChangeSequence = change_seq;
			FavouriteCount = fav_count;
			ApproverID = approver_id;
			UploaderID = uploader_id;
			CommentCount = comment_count;
			Description = description;
			Rating = rating;
			Created = created_at;
			Updated = updated_at;
			Score = score;
			Flags = flags;
			File = file;
			Preview = preview;
			Sample = sample;
			Relationship = relationships;
			poolIds = pools;
			lockedTags = locked_tags;
			Tags = tags;
			this.sources = sources;
		}
	}
}
