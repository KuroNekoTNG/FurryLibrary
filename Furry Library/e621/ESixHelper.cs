﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

using Discord;
using Discord.WebSocket;

using FluffyBot.Attributes;
using FluffyBot.Utilities;
using FluffyBot.Utilities.Extension;
using FluffyBot.Utilities.Logging;

using Newtonsoft.Json;

using ThreadState = System.Threading.ThreadState;

namespace Furry.e621 {
	internal static class ESixHelper {

		private struct MatchComparer : IComparer<Match> {
			public int Compare( Match x, Match y ) {
				int output;

				output = x.Groups[1].Value.Length - y.Groups[1].Value.Length;

				if ( output == 0 ) {
					output = y.Groups[1].Value.CompareTo( x.Groups[1].Value );
				}

				return output;
			}
		}

		#region Constants

		#region e621 and e926 URL's
		public const string WEBSITE = "https://e621.net";

		public const string WEBSITE_GET_API = "https://e621.net/posts.json?tags={0}&limit=320";

		public const string WEBSITE_GET_BEFORE_ID_API = "https://e621.net/posts.json?tags={0}&limit=320&id:{1}";

		public const string WEBSITE_SAFE = "https://e926.net";

		public const string WEBSITE_SAFE_GET_API = "https://e926.net/posts.json?tags={0}&limit=320";

		public const string WEBSITE_SAFE_GET_BEFORE_ID_API = "https://e926.net/posts.json?tags={0}&limit=320&id:{1}";
		#endregion

		#region Keys
		/// <summary>
		/// The key used to get the blacklist of tags.
		/// </summary>
		public const string BLACKLISTED_TAGS_KEY = "blacklistedTags";

		/// <summary>
		/// The key used to get the blacklist of artists.
		/// </summary>
		public const string BLACKLISTED_ARTIST_KEY = "blacklistedArtist";

		/// <summary>
		/// The key used to get the lowest allowed score to be posted.
		/// </summary>
		public const string LOWEST_ALLOWED_SCORE_KEY = "LowestAllowedScore";

		/// <summary>
		/// The key used to get the most cycles for pulling posts from e621.net.
		/// </summary>
		public const string MAXIMUM_e621_PULL_CYCLE_KEY = "MaximumE621PullCycle";
		#endregion

		#region Regex
		/// <summary>
		/// Regex to change the `locked_tags` to a proper json array.
		/// </summary>
		public static readonly Regex JSON_LOCKED_TAGS;

		/// <summary>
		/// Regex to change `tags` to a proper json array.
		/// </summary>
		public static readonly Regex JSON_TAGS;

		/// <summary>
		/// Regex to change `children` to a proper json array.
		/// </summary>
		public static readonly Regex JSON_CHILDREN;

		/// <summary>
		/// Regex for the `created_at` json tag to change it to the defacto standard.
		/// </summary>
		public static readonly Regex JSON_CREATED_AT;
		#endregion

		public static readonly string TAG_DATABASE_CACHE_LOC;

		#endregion

		public static int Requests => requests.Count;

		public static bool PullThreadRunning => pullThread.ThreadState == ThreadState.Running;
		public static bool PullThreadSleeping => pullThread.ThreadState == ThreadState.WaitSleepJoin;

		private static DateTime lastCall;

		private static FurryExtension ext;

		private static Thread pullThread;
		private static ManualResetEvent quitting, newRequest, pullingTags;
		private static ConcurrentQueue<Request> requests;
		private static JsonSerializerSettings jsonSettings;

		[SuppressMessage( "Performance", "CA1810:Initialize reference type static fields inline", Justification = "Some of those things needs logic to them." )]
		static ESixHelper() {
			JSON_LOCKED_TAGS = new Regex( "\"locked_tags\":\\s?(\"[^%,#\\\\*]+\")," );
			JSON_TAGS = new Regex( "\"tags\":\\s?(\"[^,]+\")," );
			JSON_CHILDREN = new Regex( "\"children\":\\s?(\"[0-9+,*]*\")" );
			JSON_CREATED_AT = new Regex( "\"created_at\":\\s?(\\{[\\n|\\s|\\t]*\"\\w+\":\\s?\"\\w+\",[\\n|\\s|\\t]*\"s\":\\s?([0-9]+),[\n|\\s|\t]*\"n\":\\s?[0-9]+[\\n|\\s|\\t]*\\})" );

			ext = ( FurryExtension )ModuleHandler.GetMatchingModule( typeof( FurryExtension ) );

			lastCall = DateTime.UtcNow;

			jsonSettings = SettingManager.JSON_SETTINGS;

			requests = new ConcurrentQueue<Request>();
			quitting = new ManualResetEvent( false );
			newRequest = new ManualResetEvent( false );
			pullingTags = new ManualResetEvent( true );

			pullThread = new Thread( SniffThroughQueue, 15728640 ) {
				Name = "Request Handling Thread",
				IsBackground = true,
				Priority = ThreadPriority.Lowest
			};

			pullThread.Start();
		}

		#region Settings Shortcut

		#region Artist Blacklist

		public static void SetArtistsToBlacklist( params string[] artists ) {
			var artistList = new List<string>( artists );

			if ( !ext.TrySetSetting( BLACKLISTED_ARTIST_KEY, artistList ) ) {
				_ = ext.TryAddSetting( BLACKLISTED_ARTIST_KEY, artistList );
			}
		}

		public static void AddArtistsToBlacklist( params string[] artists ) {
			if ( !ext.TryGetSetting<List<string>>( BLACKLISTED_ARTIST_KEY, out var artistBlacklist ) || artistBlacklist.Count == 0 ) {
				throw new Exception( "Cannot add artists to non-existant list. Did you mean set?" );
			}

			for ( var i = 0; i < artists.Length; ++i ) {
				var artist = artists[i];
				var pos = artistBlacklist.BinarySearch( artist );

				if ( pos >= 0 ) {
					continue;
				}

				artistBlacklist.Insert( ~pos, artist );
			}
		}

		public static void RemoveArtistsFromBlacklist( params string[] artists ) {
			if ( !ext.TryGetSetting<List<string>>( BLACKLISTED_ARTIST_KEY, out var artistBlacklist ) || artistBlacklist.Count == 0 ) {
				throw new Exception( "Cannot remove an artist when list does not exist." );
			}

			for ( var i = 0; i < artists.Length; ++i ) {
				var artist = artists[i];
				var pos = artistBlacklist.BinarySearch( artist );

				if ( pos < 0 ) {
					continue;
				}

				artistBlacklist.RemoveAt( pos );
			}
		}

		public static void ShowBlacklistedArtists( IDMChannel dm ) {
			if ( !ext.TryGetSetting<List<string>>( BLACKLISTED_ARTIST_KEY, out var artistBlacklist ) || artistBlacklist.Count == 0 ) {
				throw new Exception( "Cannot pull data from non-existant list." );
			}

			SendAListToUser( dm, artistBlacklist, "artist_blacklist.txt" );
		}

		#endregion

		#region Tag Blacklist

		public static void SetTagsAsBlacklist( string[] tags ) {
			var blacklistTags = new List<string>( tags );

			if ( !ext.TrySetSetting( BLACKLISTED_TAGS_KEY, blacklistTags ) ) {
				_ = ext.TryAddSetting( BLACKLISTED_TAGS_KEY, blacklistTags );
			}
		}

		public static void AddTagsToBlacklist( string[] tags ) {
			if ( !ext.TryGetSetting<List<string>>( BLACKLISTED_TAGS_KEY, out var blacklistTags ) || blacklistTags.Count == 0 ) {
				throw new Exception( "Cannot add tags to a non-existant list, did you mean set?" );
			}

			for ( var i = 0; i < tags.Length; ++i ) {
				var tag = tags[i];
				var pos = blacklistTags.BinarySearch( tag );

				if ( pos >= 0 ) {
					continue;
				}

				blacklistTags.Insert( ~pos, tag );
			}
		}

		public static void RemoveTagsFromBlacklist( string[] tags ) {
			if ( !ext.TryGetSetting<List<string>>( BLACKLISTED_TAGS_KEY, out var blacklistTags ) || blacklistTags.Count == 0 ) {
				throw new Exception( "Cannot remove tags from a non-existant list." );
			}

			for ( var i = 0; i < tags.Length; ++i ) {
				var tag = tags[i];
				var pos = blacklistTags.BinarySearch( tag );

				if ( pos < 0 ) {
					continue;
				}

				blacklistTags.RemoveAt( pos );
			}
		}

		public static void ShowBlacklistedTags( IDMChannel dm ) {
			if ( !ext.TryGetSetting<List<string>>( BLACKLISTED_TAGS_KEY, out var blacklistTags ) || blacklistTags.Count == 0 ) {
				throw new Exception( "Cannot pull data from a non-existant list." );
			}

			SendAListToUser( dm, blacklistTags, "tag_blacklist.txt" );
		}

		#endregion

		#region Lowest Score

		public static void SetLowestAllowedScore( long score ) {
			if ( !ext.TrySetSetting( LOWEST_ALLOWED_SCORE_KEY, score ) ) {
				_ = ext.TryAddSetting( LOWEST_ALLOWED_SCORE_KEY, score );
			}
		}

		public static void ShowLowestAllowedScore( IDMChannel dm ) {
			var builder = new EmbedBuilder();

			if ( !ext.TryGetSetting<long>( LOWEST_ALLOWED_SCORE_KEY, out var score ) ) {
				score = long.MinValue;
			}

			builder.AddField( "Lowest Allowed Score", score );
			builder.Color = Color.Blue;

			dm.SendMessageAsync( embed: builder.Build() );
		}

		#endregion

		#region Pull Limit
		public static void SetLimitOfPulls( ulong limit = ulong.MinValue ) {
			if ( !ext.TrySetSetting( MAXIMUM_e621_PULL_CYCLE_KEY, limit ) ) {
				_ = ext.TryAddSetting( MAXIMUM_e621_PULL_CYCLE_KEY, limit );
			}
		}

		public static void ShowLimitOfPulls( IDMChannel dm ) {
			var builder = new EmbedBuilder();

			if ( !ext.TryGetSetting( MAXIMUM_e621_PULL_CYCLE_KEY, out ulong limit ) ) {
				limit = ulong.MinValue;

				if ( !ext.TryAddSetting( MAXIMUM_e621_PULL_CYCLE_KEY, ulong.MinValue ) ) {
					throw new Exception( "Unknown problem occured within. Settings claim key-value isn't in settings but is in settings." );
				}
			}

			builder.AddField( "Maximum Number of Pulls", limit == 0 ? ( object )"Unlimited" : limit );
			builder.Color = Color.Blue;

			dm.SendMessageAsync( embed: builder.Build() );
		}
		#endregion

		#endregion

		public static List<string> GetTagBlacklist() {
			_ = ext.TryGetSetting( BLACKLISTED_TAGS_KEY, out List<string> tagBlacklist );

			return tagBlacklist;
		}

		public static List<string> GetArtistBlacklist() {
			_ = ext.TryGetSetting( BLACKLISTED_ARTIST_KEY, out List<string> artistBlacklist );

			return artistBlacklist;
		}

		public static void AddRequest( SocketUser user, IMessageChannel channel, Action<List<Post>, Request> callback, params string[] tags ) {
			var request = new PostRequest( user, channel, callback, tags );

			requests.Enqueue( request );

			newRequest.Set();
		}

		[HandleLinkInMessage( "e621.net/post/show", "e926.net/post/show" )]
		public static void DealWithE621Links( SocketUserMessage channel, string link ) {
			string newLink;

			LogHandler.Log( $"Recieved message about `{link}`." );

			newLink = Regex.Match( link, @"(https?://e(?:621|926)\.net/post/show/[0-9]*)[^\s]*\s?" ).Groups[1].Value;

			requests.Enqueue( new LinkRequest( channel, HandleLink, newLink ) );

			newRequest.Set();
		}

		public static Embed BuildPostEmbed( Post post, bool useE9 = false ) {
			string descString;
			Embed embed;
			var embedBuilder = new EmbedBuilder {
				ImageUrl = post.File.URL.ToString(), // Makes the Image URL the path to the file.
				ThumbnailUrl = post.Preview.URL.ToString(), // Makes the thumbnail url the path to the prieview.
				Color = Color.DarkBlue, // Makes the color of the embed dark blue.
				Url = post.File.URL.ToString(), // Makes the url of the post link to the post.
				Timestamp = post.Created // Makes the timestamp the time the post was created.
			};

			#region Adding Fields
			embedBuilder.AddField(
					"Score",
					post.Score.Total,
					true
				);

			embedBuilder.AddField(
				"File Type",
				post.File.Extension,
				true
			);

			embedBuilder.AddField(
				"Width",
				post.File.Width,
				true
			);

			embedBuilder.AddField(
				"Height",
				post.File.Height,
				true
			);

			embedBuilder.AddField(
				"File Size",
				post.File.Size,
				true
			);

			embedBuilder.AddField(
				"Link",
				$"[{( useE9 ? "e926.net" : "e621.net" )}]({post.PostURL})",
				true
			);
			#endregion

			if ( !string.IsNullOrWhiteSpace( post.Description ) ) { // Checks if the post has a description.
				if ( post.Description.Length > 1024 ) {
					descString = post.Description.Substring( 0, 1021 ) + "...";
				} else {
					descString = post.Description;
				}

				embedBuilder.Description = descString;
			}

			embed = embedBuilder.Build();

			LogHandler.Log( $"New Embed, looks like:\n{embed.ToString()}" );

			return embed;
		}

		private static void HandleLink( SocketUser user, IMessageChannel channel, string link, ulong messageId ) {
			var userMessage = ( IUserMessage )channel.GetMessageAsync( messageId ).GetAwaiter().GetResult();

			HandleLink( userMessage, link );
		}

		private static void HandleLink( IUserMessage userMessage, string link ) {
			string json;
			Post post;

			var useE9 = false;
			var channel = ( ITextChannel )userMessage.Channel;
			var typing = channel.EnterTypingState();

			try {
				json = PullFromSite( link.Trim() + ".json" );
			} catch ( WebException e ) {
				throw new Exception( $"e621 link: {link}\nDiscord Link: {userMessage.GetJumpUrl()}", e );
			}

			post = JsonConvert.DeserializeObject<Post>( json, jsonSettings );

			if ( IsPostBlacklisted( post ) ) {
				UserFault( false );

				typing.Dispose();

				return;
			}

			if ( !channel.IsNsfw ) {
				if ( post.Rating != Rating.Safe ) {
					UserFault( true );

					typing.Dispose();

					return;
				}

				useE9 = true;
			}

			userMessage.Channel.SendMessageAsync( $"{userMessage.Author.Mention} Here's a better look at it!", embed: BuildPostEmbed( post, useE9 ) );

			typing.Dispose();

			void UserFault( bool badUrl ) {
				const string BAD_URL = "{0} You have posted an NSFW post in a SFW channel!";
				const string BAD_TAG = "{0} ÒmÓ You have posted a link to a blacklisted post!";

				channel.SendMessageAsync( badUrl ? BAD_URL.Format( userMessage.Author.Mention ) : BAD_TAG.Format( userMessage.Author.Mention ) );
				userMessage.DeleteAsync();
			}
		}

		private static void SniffThroughQueue() {
			int action;

			while ( true ) {
				action = WaitHandle.WaitAny( new[] { newRequest, quitting } );

				switch ( action ) {
					case 0:
						BeginRequests();
						newRequest.Reset();
						break;
					case 1:
						quitting.Reset();

						BeginRequests();

						return;
					default:
						LogHandler.LogWarning( $"Unknown action type: `{action}`." );
						break;
				}
			}
		}

		private static string PullFromSite( string link ) {
			string json;

			using ( var client = new WebClient() ) {
				client.Headers.Add( HttpRequestHeader.UserAgent, "FluffyBot/KuroNeko-TNG" );

				var wait = DateTime.UtcNow - lastCall;

				if ( wait.TotalSeconds < 2 ) {
					Thread.Sleep( 2000 - ( int )wait.TotalMilliseconds );
				}

				json = client.DownloadString( link );
			}

			lastCall = DateTime.UtcNow;

			if ( string.IsNullOrWhiteSpace( json ) ) {
				json = "[]";
			}

			return json;
		}

		private static void BeginRequests() {
			LogHandler.Log( $"Request queue has {requests.Count} requests queued." );

			while ( requests.TryPeek( out var request ) ) {
				pullingTags.WaitOne();

				if ( request is PostRequest postRequest ) {
					var posts = GetPosts( postRequest );

					FilterPosts( posts );

					request.Callback?.Invoke( posts, request );
				} else if ( request is LinkRequest linkRequest ) {
					linkRequest.Callback?.Invoke(
						linkRequest.User, // This is the user that made the message.
						linkRequest.MessageChannel, // This is the channel it took place in.
						linkRequest.Link, // This is the link they posted.
						linkRequest.MessageId // This is the message id to find the message.
					);
				}

				if ( !requests.TryDequeue( out _ ) ) {
					throw new Exception( $"Attempted to dequeue `{request.GetType().FullName}` from the start, but appears to already be gone." );
				}
			}

			LogHandler.Log( "Done with request queue." );
		}

		private static void FilterPosts( List<Post> posts ) {
			for ( var i = 0; i < posts.Count; ++i ) {
				LogHandler.Log( $"On `{i}` of `{posts.Count}` of internal list of posts.\nPercent done: { ( double )i / posts.Count:P2}" );

				if ( IsPostBlacklisted( posts[i] ) ) {
					posts.RemoveAt( i-- );
					continue;
				}

				LogHandler.Log( $"Post `{posts[i].ID}` made it past both blacklists." );
			}
		}

		private static bool IsPostBlacklisted( Post post ) {
			if ( !ext.TryGetSetting( BLACKLISTED_ARTIST_KEY, out List<string> artistBlacklist ) ) {
				LogHandler.LogWarning( "No artist blacklist returned." );

				ext.TryAddSetting( BLACKLISTED_ARTIST_KEY, new List<string>() );
			}

			if ( !ext.TryGetSetting( BLACKLISTED_TAGS_KEY, out List<string> tagBlacklist ) ) {
				LogHandler.LogWarning( "No tag blacklist returned." );

				ext.TryAddSetting( BLACKLISTED_TAGS_KEY, new List<string>() );
			}

			if ( !ext.TryGetSetting( LOWEST_ALLOWED_SCORE_KEY, out long lowestScore ) ) {
				LogHandler.LogWarning( "Defaulting lowest score to minimum." );

				lowestScore = long.MinValue;

				ext.TryAddSetting( LOWEST_ALLOWED_SCORE_KEY, lowestScore );
			}

			var postArtist = post.Tags.Artist.ToArray();
			var postTags = post.Tags.AllTagsWithoutArtist.ToArray();

			for ( int i = 0; i < postArtist.Length; ++i ) {
				if ( artistBlacklist.BinarySearch( postArtist[i] ) >= 0 ) {
					return true;
				}
			}

			for ( int i = 0; i < postTags.Length; ++i ) {
				if ( tagBlacklist.BinarySearch( postTags[i] ) >= 0) {
					return true;
				}
			}

			if ( post.Score.Total < lowestScore ) {
				return true;
			}

			return false;
		}

		private static List<Post> GetPosts( PostRequest request ) {
			var posts = new List<Post>();
			uint cycles = 0;
			ulong maximumCycles;

			do {
				string json;

				// Allows the bot to be able to adapt when settings change.
				if ( !ext.TryGetSetting( MAXIMUM_e621_PULL_CYCLE_KEY, out maximumCycles ) ) {
					LogHandler.LogWarning( "Limit the pull cycle you porn addicted furry." );
				}

				var url = string.Empty;

				if ( posts.Count == 0 ) {
					url = string.Format(
						WEBSITE_GET_API,
						TagsToQuery( request.Tags )
					);
				} else {
					url = string.Format(
						WEBSITE_GET_BEFORE_ID_API, // The string to format.
						TagsToQuery( request.Tags ), // Since the tags are not in a string, make them.
						posts[posts.Count - 1].ID // Since the site will ignore the before_id when it is 0, just add that.
					);
				}

				try {
					json = PullFromSite( url );
				} catch ( Exception e ) {
					LogHandler.LogError( "Something happened while pulling from e621.net.", e );

					break;
				}

				var startJsonArray = json.IndexOf( '[' );
				var length = json.LastIndexOf( ']' ) - startJsonArray + 1;

				json = json.Substring( startJsonArray, length );

				if ( json == "null" || json == "[]" ) {
					break;
				}

				try {
					posts.AddRange( JsonConvert.DeserializeObject<IList<Post>>( json, jsonSettings ) );
				} catch ( Exception e ) {
					LogHandler.LogError( $"Something happened while either adding posts to list, or deserializing.\nThe json:\n{json}", e );

					break;
				}

				++cycles;
			} while ( maximumCycles == 0 || cycles < maximumCycles );

			return posts;
		}

		private static string TagsToQuery( IList<string> tags ) {
			var tagBuilder = new StringBuilder();

			// The reason for a double check of a constant limit and the method field
			// is that the e621.net only accepts up to 6 tags before returning a failure.
			for ( var i = 0; i < tags.Count && i < 6; ++i ) {
				tagBuilder.Append( tags[i] );

				if ( i < tags.Count - 1 ) {
					tagBuilder.Append( '+' );
				}
			}

			return tagBuilder.ToString();
		}

		// An easy way to send a list setting to the user.
		private static void SendAListToUser( IDMChannel dm, IList<string> list, string filename, string start = "" ) {
			var builder = new StringBuilder( start );
			byte[] bytes;

			for ( var i = 0; i < list.Count; ++i ) {
				builder.Append( list[i] );

				if ( i < list.Count - 1 ) {
					builder.Append( ",\n" );
				}
			}

			// Since the memory stream requires bytes, this is used.
			bytes = Encoding.UTF8.GetBytes( builder.ToString() );

			using ( var memStream = new MemoryStream( bytes ) ) {
				// Since the stream should not be destroyed before sending the message is done,
				// this forces the code to wait for the sending to complete.
				dm.SendFileAsync( memStream, filename ).Wait();
			}
		}
	}
}