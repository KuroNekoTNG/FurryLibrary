﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage(
	"Design",
	"CA1031:Do not catch general exception types",
	Justification = "Needed to make the program run smoothly."
)]
[assembly: SuppressMessage(
	"Performance",
	"CA1819:Properties should not return arrays",
	Justification = "Do not care as those lists will also be set."
)]
[assembly: SuppressMessage(
	"Performance",
	"CA1815:Override equals and operator equals on value types",
	Justification = "No",
	Scope = "type",
	Target = "~T:Furry.e621.Flags"
)]
[assembly: SuppressMessage(
	"Performance",
	"CA1815:Override equals and operator equals on value types",
	Justification = "<Pending>",
	Scope = "type",
	Target = "~T:Furry.e621.Score"
)]
