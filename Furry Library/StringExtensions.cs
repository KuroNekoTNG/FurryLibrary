﻿using System;

public static class StringExtensions {

	public static string Format( this string str, params string[] args ) => Format( str, ( object[] )args );

	public static string Format( this string str, IFormatProvider formatProvider, params string[] args ) => Format( str, formatProvider, args );

	public static string Format( this string str, params object[] args ) => string.Format( str, args );

	public static string Format( this string str, IFormatProvider formatProvider, params object[] args ) => string.Format( formatProvider, str, args );
}