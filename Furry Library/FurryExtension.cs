﻿using System.Diagnostics;

using FluffyBot.Utilities.Extension;

namespace Furry {
	public class FurryExtension : Module {

		[Conditional( "DEBUG" )]
		private static void AttachDebugger() {
			if ( !Debugger.IsAttached ) {
				Debugger.Launch();
			}
		}

		public FurryExtension() : base() {
			AttachDebugger();
		}
	}
}
