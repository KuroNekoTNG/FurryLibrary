﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Discord;
using Discord.Commands;
using Discord.WebSocket;

using FluffyBot.Core;
using FluffyBot.Utilities.Logging;

using Furry.e621;

namespace Furry {
	public sealed class FurryCommands : CommandCore {

		/// <summary>
		/// The default contructor that initializes all the fields.
		/// </summary>
		public FurryCommands() {

		}

		#region Blacklist
		[Command( "blacklistArtists", RunMode = RunMode.Async )]
		public async Task ModifyArtistBlacklist( string action, params string[] artists ) {
			var builder = new StringBuilder(); // Creates a new string builder.
			var userDM = await ( ( SocketGuildUser )Context.User ).GetOrCreateDMChannelAsync(); // Grabs the direct message channel to the owner.

			switch ( action ) { // For what action to do.
				case "add": // For when the owner wishes to add an artist(s) to the blacklist.
					if ( ( ( SocketGuildUser )Context.User ).GuildPermissions.Administrator ) { // Checks to see if the user is an admin.
						ESixHelper.AddArtistsToBlacklist( artists ); // Adds the artist to the blacklist. 
					} else {
						await userDM.SendMessageAsync( "You are not allowed to do this as you are not an admin." ); // Informs the user that they are not allowed to do this.
					}

					break; // Breaks out of the switch.
				case "remove": // For when the owner wishes to remove artist(s) from the blacklist.
					if ( ( ( SocketGuildUser )Context.User ).GuildPermissions.Administrator ) { // Checks to see if the user is an admin.
						ESixHelper.RemoveArtistsFromBlacklist( artists ); // Calls the method that will remove artists from the blacklist. 
					} else {
						await userDM.SendMessageAsync( "You are not allowed to do this as you are not an admin." ); // Informs the user that they are not allowed to do this.
					}

					break; // Breaks out of the switch statement.
				case "set": // For when the owner wishes to set the blacklist to the one they typed.
					if ( ( ( SocketGuildUser )Context.User ).GuildPermissions.Administrator ) { // Checks to see if the user is an admin.
						ESixHelper.SetArtistsToBlacklist( artists ); // Calls the method to set the blacklist of artists. 
					} else {
						await userDM.SendMessageAsync( "You are not allowed to do this as you are not an admin." ); // Informs the user that they are not allowed to do this.
					}

					break; // Breaks out of the switch.
				case "show":
					ESixHelper.ShowBlacklistedArtists( userDM );
					return;
				default:
					await userDM.SendMessageAsync( string.Format( "Failed to {0}. It is probably an unknown command.", action ) ); // Sends a message the to owner saying it failed.
					return; // Breaks out of the method.
			}

			for ( var i = 0; i < artists.Length; ++i ) { // Loops through all the artists.
				builder.Append( artists[i] ); // Appends the artist to the string.

				if ( i < artists.Length - 1 ) { // Checks to see if it is on the last one.
					builder.Append( ", " ); // Appends a comma to seperate previous entry with the next.
				}
			}

			// Tells the owner that the command completed successfully.
			await userDM.SendMessageAsync( string.Format( "Successfully {0} with the following artists from the blacklist:\n{1}", action, builder.ToString() ) );
		}

		[Command( "blacklistTags", RunMode = RunMode.Async )]
		public async Task ModifyBlacklistedTags( string action, params string[] tags ) {
			var builder = new StringBuilder();
			var userDM = await ( ( SocketGuildUser )Context.User ).GetOrCreateDMChannelAsync(); // Get's or creates that DM.

			switch ( action ) { // Switches on the action.
				case "add":
					if ( ( ( SocketGuildUser )Context.User ).GuildPermissions.Administrator ) { // Checks to see if the user is an admin.
						ESixHelper.AddTagsToBlacklist( tags ); // Calls the method that adds the tags to the blacklist. 
					} else {
						await userDM.SendMessageAsync( "You are not allowed to do this as you are not an admin." ); // Informs the user that they are not allowed to do this.
					}

					break;
				case "remove":
					if ( ( ( SocketGuildUser )Context.User ).GuildPermissions.Administrator ) { // Checks to see if the user is an admin.
						ESixHelper.RemoveTagsFromBlacklist( tags ); // Calls the method that will remove the tags. 
					} else {
						await userDM.SendMessageAsync( "You are not allowed to do this as you are not an admin." ); // Informs the user that they are not allowed to do this.
					}

					break;
				case "set":
					if ( ( ( SocketGuildUser )Context.User ).GuildPermissions.Administrator ) { // Checks to see if the user is an admin.
						ESixHelper.SetTagsAsBlacklist( tags ); // Calls the method that will set these tags as the blacklist. 
					} else {
						await userDM.SendMessageAsync( "You are not allowed to do this as you are not an admin." ); // Informs the user that they are not allowed to do this.
					}

					break;
				case "show":
					ESixHelper.ShowBlacklistedTags( userDM ); // Calls the method that will report the blacklisted items to the owner.
					return;
				default:
					await userDM.SendMessageAsync( string.Format( "Failed to {0}. It is probably an unknown command.", action ) ); // Sends a message the to owner saying it failed.
					return;
			}

			for ( var i = 0; i < tags.Length; ++i ) { // Loops through all the artists.
				builder.Append( tags[i] ); // Appends the artist to the string.

				if ( i < tags.Length - 1 ) { // Checks to see if it is on the last one.
					builder.Append( ", " ); // Appends a comma to seperate previous entry with the next.
				}
			}

			// Tells the owner that the command completed successfully.
			await userDM.SendMessageAsync( string.Format( "Successfully {0} with the following tags from the blacklist:\n{1}", action, builder.ToString() ) );
		}
		#endregion

		[Command( "lowestPossibleScore" )]
		public async Task LowestPossibleScoreCommandHandler( string action, long score = long.MinValue ) {
			var guildUser = ( SocketGuildUser )Context.User;
			var dm = await guildUser.GetOrCreateDMChannelAsync();

			switch ( action.ToLower() ) {
				case "set":
					if ( !guildUser.GuildPermissions.Administrator ) {
						await dm.SendMessageAsync( "You do not have permission to do this." );
						return;
					}

					ESixHelper.SetLowestAllowedScore( score );
					return;
				case "show":
					ESixHelper.ShowLowestAllowedScore( dm );
					return;
				default:
					await dm.SendMessageAsync( "I do not understand what you have asked for." );
					return;
			}
		}

		[Command( "e621MaximumCycles" )]
		public async Task MaximumE621PullCycles( string action, uint cycles = uint.MinValue ) {
			var guildUser = ( SocketGuildUser )Context.User;
			var dm = await guildUser.GetOrCreateDMChannelAsync();

			switch ( action.ToLower() ) {
				case "set":
					if ( !guildUser.GuildPermissions.Administrator ) {
						await dm.SendMessageAsync( "You do nto have permission to do this." );
						return;
					}

					ESixHelper.SetLimitOfPulls( cycles );
					return;
				case "show":
					ESixHelper.ShowLimitOfPulls( dm );
					return;
				default:
					await dm.SendMessageAsync( "I do not understand what you have asked for." );
					return;
			}
		}
		/// <summary>
		/// Grabs porn off e621.net.
		/// </summary>
		/// <param name="tags">An array of tags.</param>
		/// <returns></returns>
		[Command( "e621", RunMode = RunMode.Async ), Alias( "esix21", "e6two1", "e62one", "esixtwo1", "esix2one", "e6twoone", "esixtwoone" ), RequireNsfw]
		public Task PornFrome621( params string[] tags ) => CreateRequest( SendPorn, tags ); // Calls this method with the SendPorn as the callback.

		[Command( "e926", RunMode = RunMode.Async ), Alias( "enine26", "e9two6", "e92six", "eninetwo6", "enine2six", "e9twosix", "eninetwosix" )]
		public Task SfwFrome621( params string[] tags ) => CreateRequest( SendArtwork, tags ); // Calls this method with the SendArtwork as the callback.

		[SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Needed to ensure that the program runs smoothly." )]
		private async Task CreateRequest( Action<List<Post>, Request> callback, params string[] tags ) {
			string message;
			var tagBlacklist = ESixHelper.GetTagBlacklist();
			var artistBlacklist = ESixHelper.GetArtistBlacklist();

			LogHandler.Log( "I am creating an e621/926 request." );

			if ( tags.Length > 6 ) {
				for ( var i = 0; i < tags.Length; i += 6 ) {
					var subTags = new LinkedList<string>();

					for ( var j = i; j < tags.Length && j < i + 6; ++j ) {
						subTags.AddLast( tags[j] );
					}

					await CreateRequest( callback, subTags.ToArray() );
				}

				return;
			}

			foreach ( var tag in tags ) {
				if ( ( tagBlacklist != null && tagBlacklist.Contains( tag ) ) || ( artistBlacklist != null && artistBlacklist.Contains( tag ) ) ) {
					_ = await Context.Channel.SendMessageAsync( string.Format( "{0}, your request contains invalid tags.", Context.User ) );
					return;
				}
			}

			if ( ESixHelper.Requests == 0 ) { // Checks if there are anything in the queue.
				message = string.Format( "{0}, your request is being processed now.", Context.User.Mention ); // Creates a new message stating that the request is processing now.
			} else {
				message = string.Format( "{0}, your request is {1}.", Context.User.Mention, ESixHelper.Requests ); // Creates a new message stating that their request is in the xth location.
			}

			try {
				ESixHelper.AddRequest( Context.User, Context.Channel, callback, tags ); // Attempts to add the request to the queue.
			} catch ( Exception e ) {
				LogHandler.LogError( "Something happened while adding a new request.", e ); // Logs the exception that is thrown.
				return; // Breaks out of the method.
			}

			_ = await Context.Channel.SendMessageAsync( string.Format( message, Context.User.Mention ) );
		}

		private void SendArtwork( List<Post> posts, Request _ ) => FilterAndSendPosts( Rating.Safe, posts ); // Calls this method to filter and post.

		private void SendPorn( List<Post> posts, Request _ ) => FilterAndSendPosts( Rating.Explicit, posts ); // Calls this method to filter and post.

		private void FilterAndSendPosts( Rating rating, List<Post> posts ) {
			var random = new Random(); // Creates a random class. Why do this? Because that's how Microsoft designed it.
			Embed embed;
			Post post;

			for ( var i = 0; i < posts.Count; ++i ) { // Loops through all the posts.

				// Since swf files are not support by Discord, and will be depricated, exclude those.
				// Also, filter based on if the post rating does not contain the desired rating.
				if ( !posts[i].Rating.HasFlag( rating ) || posts[i].File.Extension.Equals( "swf", StringComparison.OrdinalIgnoreCase ) ) {
					posts.RemoveAt( i-- ); // Removes the post at that location and decriments the index.
				}
			}

			if ( posts.Count == 0 ) {
				Context.Channel.SendMessageAsync( $"Sorry {Context.User.Mention}, it appears as if something happened. No porn for you!" );
				return;
			}

			post = posts[random.Next( posts.Count )]; // Grabs a random posts.

			embed = ESixHelper.BuildPostEmbed( post );

			// Sends the embed and notifies the user.
			_ = Context.Channel.SendMessageAsync( $"Master {Context.User.Mention}, here's what you requested.", embed: embed )
				.GetAwaiter().GetResult();

			LogHandler.Log( $"The url is `{post.File.URL.ToString()}`." );
		}
	}
}